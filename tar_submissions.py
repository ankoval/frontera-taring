#!/usr/bin/env python3

from sys import stdin, stderr
from os.path import join
from typing import List, Generator
from os import lstat, getcwd, makedirs
from getpass import getuser
from argparse import ArgumentParser
from string import ascii_lowercase
from random import sample
from logging import DEBUG, INFO, Formatter, StreamHandler, getLogger

logger = getLogger(__file__)
logger.setLevel(INFO)

ch = StreamHandler(stream=stderr)
ch.setLevel(DEBUG)
ch.setFormatter(Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
logger.addHandler(ch)


FileGroup = List[str]
GB_MULTIPLIER = 2**30
TEMPLATE_NAME = "tpl_tar_job.sh"


class FileGroupBuilder:
    def __init__(self, max_size: int) -> None:
        self._max_size = max_size
        self._current_size = 0
        self._files = []

    def will_fit(self, fsize: int) -> bool:
        return self._current_size + fsize < 2 * self._max_size - self._current_size

    def add(self, fname: str, fsize: int) -> None:
        self._files.append(fname)
        self._current_size += fsize

    def list(self) -> FileGroup:
        return self._files


def split_files(filenames: FileGroup, split_size: int) -> List[FileGroup]:
    groups = [FileGroupBuilder(split_size)]
    for fname in filenames:
        fsize = lstat(fname).st_size
        for group in groups:
            if group.will_fit(fsize):
                group.add(fname, fsize)
                break
        else:
            new_group = FileGroupBuilder(split_size)
            new_group.add(fname, fsize)
            groups.append(new_group)

    return [group.list() for group in groups]


def generate_submissions(
    filenames: FileGroup,
    split_size: int,
    output_dir: str,
    base_dir: str = getcwd(),
    job_prefix: str = "tar",
    execution_time: int = 60 * 30,
 ) -> Generator[str, None, None]:
    tpl = open(TEMPLATE_NAME).read()
    submission_index = 0

    file_groups = split_files(filenames, split_size)
    file_groups_len = len(file_groups)
    for group in file_groups:
        submission_index += 1
        job_name = "{}.{:0>3}".format(job_prefix, submission_index)

        submission_content = tpl.format(
            job_name=job_name,
            job_stdout=join(output_dir, job_name + ".stdout"),
            job_stderr=join(output_dir, job_name + ".stderr"),
            execution_time=execution_time,
            username=getuser(),
            project_name="PHY20003",
            workers_number=1,
            idx=job_name,
            base_dir=base_dir,
            target_name=job_name,
            target_files=" ".join(group),
            output_dir=output_dir,
        )
        submission_path = join(base_dir, "submissions", job_name + ".sh")
        logger.info(f"Job {submission_index} / {file_groups_len}: {job_name} at {submission_path}")

        with open(submission_path, "w") as output:
            output.write(submission_content)

        yield submission_path

def main() -> None:
    aparser = ArgumentParser(
        description="Generate submission files for tarring files.",
    )
    aparser.add_argument("--base-dir", type=str, default=getcwd(), help="Basic directory with configuration. Default: {}".format(getcwd()))
    aparser.add_argument("--prefix", type=str, default="tar-" + "".join(sample(ascii_lowercase, 10)), help="Submission prefix. Default is tar_ + random string")
    aparser.add_argument("--output-dir", type=str, default=getcwd(), help="Place to transfer tars after done")
    aparser.add_argument("--tar-size", type=int, default=150, help="Approximate size of the final tar in GB. Default: 150 G")
    aparser.add_argument("--exec-time", type=int, default=1800, help="Requested wall-time in seconds. Default: 1800 s")
    args = aparser.parse_args()



    input_files = []
    for filename in stdin:
        input_files.append(filename.strip())

    input_files_len = len(input_files)

    basedir = args.base_dir
    split_size=args.tar_size
    output_dir=args.output_dir
    base_dir=basedir
    job_prefix=args.prefix
    exec_time=args.exec_time

    logger.info(f"""Arguments:
--base-dir="{basedir}" \
--prefix="{job_prefix}" \
--output_dir="{output_dir}" \
--tar-size="{split_size}" \
--exec-time={exec_time}"

Total input files: {input_files_len}
""")

    submissions_dir = join(base_dir, "submissions")
    makedirs(submissions_dir, exist_ok=True)
    logger.info(f"Checked {submissions_dir}")

    subs =list(generate_submissions(
        filenames=input_files,
        split_size=split_size * GB_MULTIPLIER,
        output_dir=output_dir,
        base_dir=base_dir,
        job_prefix=job_prefix,
        execution_time=exec_time,
    ))

    logger.info("Done")



if __name__ == "__main__":
    main()

