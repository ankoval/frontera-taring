#!/usr/bin/env python3

from datetime import datetime
from getpass import getuser
from os import listdir
from os.path import basename, dirname, exists, join
from subprocess import PIPE, Popen
from typing import Dict, List, Tuple

MAX_JOBS_ALLOWED = 200


def get_queue_stats() -> Dict[str, List[str]]:
    username = getuser()
    proc = Popen(f'squeue -u {username}', shell=True, stdout=PIPE, stderr=PIPE)
    out, err = proc.communicate()
    out_decoded = out.decode('utf-8')
    stats = dict(run=[], wait=[], total=[], cancel=[])
    for line in out_decoded.splitlines():
        if username not in line:
            continue

        stats['total'].append(line)
        status = line.split()[4]
        if status == 'R':
            stats['run'].append(line)
        elif status == 'PD':
            stats['wait'].append(line)
        elif status == 'CG':
            stats['cancel'].append(line)

    return stats


def marker_name(filepath) -> str:
    return join(dirname(filepath), '.' + basename(filepath) + '.started')


def list_run_dirs() -> List[str]:
    res = []
    for name in listdir('.'):
        if name.startswith('run_'):
            res.append(name)
    return res


def submit_job(filepath) -> None:
    proc = Popen(f'sbatch {filepath}', shell=True, stdout=PIPE, stderr=PIPE)
    out, err = proc.communicate()
    retcode = proc.returncode

    if not retcode:
        with open(marker_name(filepath), 'w') as marker:
            marker.write(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))


def get_jobs_stats() -> Tuple[List[str], List[str]]:
    submitted = []
    not_submitted = []
    for file in listdir('submissions/'):
        if not file.endswith('.sh'):
            continue

        filename = join('submissions', file)
        if exists(marker_name(filename)):
            submitted.append(filename)
        else:
            not_submitted.append(filename)

    return submitted, not_submitted


if __name__ == '__main__':
    queue_stats = get_queue_stats()
    submitted, not_submitted = get_jobs_stats()

    print('Total in queue: {: >5}'.format(len(queue_stats['total'])))
    print('Total running : {: >5}'.format(len(queue_stats['run'])))
    print('Total canceled: {: >5}'.format(len(queue_stats['cancel'])))
    print('Total waiting : {: >5}'.format(len(queue_stats['wait'])))
    print('')

    completed_jobs = len(list_run_dirs())
    # print('Expecting     : {: >5}'.format(len(submitted) - completed_jobs))
    # print('Completed     : {: >5}'.format(completed_jobs))
    print('Submitted     : {: >5}'.format(len(submitted)))
    print('Not submitted : {: >5}'.format(len(not_submitted)))
    print('')

    for job in queue_stats['run']:
        print(job)

    current_queue_size = len(queue_stats['total'])
    queue_growth = MAX_JOBS_ALLOWED - current_queue_size
    print('')
    print(f'Adding {queue_growth} new jobs to the queue: ', end='')
    for job in not_submitted[:queue_growth]:
        submit_job(job)
        print('.', end='')

    print('')
    print('Done!')
