#!/usr/bin/env bash
#SBATCH -J {job_name}
#SBATCH -o {job_stdout}
#SBATCH -e {job_stderr}
#SBATCH -p small
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -t {execution_time}
#SBATCH --mail-type=all
#SBATCH --mail-user={username}
#SBATCH -A {project_name}

export OMP_NUM_THREADS={workers_number}
export JOB_PREFIX="{idx}"
export JOB_DESTINATION="{base_dir}"
mkdir "/tmp/${{JOB_PREFIX}}"

current_dir="${{PWD}}"

module use /work/01255/siliu/stampede2/ooops/modulefiles/
module load python3
module load ooops
export IO_LIMIT_CONFIG=/work/01255/siliu/stampede2/ooops/1.0/conf/config_low
set_io_param 0 low

cd /tmp/${{JOB_PREFIX}}/

output_tar="{output_dir}/{target_name}.tar"
tar -cvf ${{output_tar}} {target_files} > ${{output_tar}}.log

if ! tar tf "${{output_tar}}" &> "${{output_tar}}.content"; then
	touch "${{output_tar}}.bad"
else
	touch "${{output_tar}}.good"
fi
