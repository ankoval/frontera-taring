# Tarring job generator for the Frontera

To tar up the bulk of files there are a few simple steps to go with.

## Generate submissions

Say, you have a list of your files to tar in a file "files_to_tar.txt", then you run `tar_submissions.py` like:


```bash
./tar_submissions.py --prefix boromir.batch < files_to_tar.txt
```

Full list of arguments is available via `./tar_submissions.py --help`:

```
usage: tar_submissions.py [-h] [--base-dir BASE_DIR] [--prefix PREFIX]
                          [--output-dir OUTPUT_DIR] [--tar-size TAR_SIZE]
                          [--exec-time EXEC_TIME]

Generate submission files for tarring files.

optional arguments:
  -h, --help            show this help message and exit
  --base-dir BASE_DIR   Basic directory with configuration. Default:
                        <your current directory>
  --prefix PREFIX       Submission prefix. Default is tar_ + random string
  --output-dir OUTPUT_DIR
                        Place to transfer tars after done
  --tar-size TAR_SIZE   Approximate size of the final tar in GB. Default: 150G
  --exec-time EXEC_TIME
                        Requested wall-time in seconds. Default: 1800s
```

This will create folder `submissions` with a bunch of submission files.


## Keep up the queue

To smoothly burn through all submissions without breaking a sweat use:

```bash
watch -n60 ./keep_up_queue.py
```

This will keep number of submitted jobs on the level of 200 (if possible) and will constantly fill up free slots.

When all the jobs would be done the output will look like somehting like this:

```
Total in queue:     0
Total running :     0
Total canceled:     0
Total waiting :     0

Submitted     :   752
Not submitted :     0


Adding 200 new jobs to the queue:
Done!
```

### The unsubmittables

For the reason not known to me some submissions are failing at one time but work out later without any visible difference except for the time of submittion. In this case the output will look like this:

```
Total in queue:     0
Total running :     0
Total canceled:     0
Total waiting :     0

Submitted     :   752
Not submitted :    68


Adding 68 new jobs to the queue:
Done!
```

Notice, there is `Not submitted :    68` and `Adding 68 new jobs ...`, but `Total in queue:     0`. In this case I can only recommend to try your luck with this later.

## Output check

As the final output for each tar you will get:

```
-rw-rw-r--+  1 akoval G-821143  63046287360 May  7 07:40 dvcs.2016.P07.mu.plus.408.tar
-rw-rw-r--+  1 akoval G-821143        49291 May  8 06:58 dvcs.2016.P07.mu.plus.408.tar.content
-rw-rw-r--+  1 akoval G-821143            0 May  7 05:42 dvcs.2016.P07.mu.plus.408.tar.good
-rw-rw-r--+  1 akoval G-821143        49291 May  8 06:58 dvcs.2016.P07.mu.plus.408.tar.log
-rw-rw-r--+  1 akoval G-821143           44 May  7 02:02 dvcs.2016.P07.mu.plus.408.stderr
-rw-rw-r--+  1 akoval G-821143           44 May  7 05:42 dvcs.2016.P07.mu.plus.408.stdout
```

if everything went ok (indicated by presence of `dvcs.2016.P07.mu.plus.408.tar.good`), or


```
-rw-rw-r--+  1 akoval G-821143  63046287360 May  7 07:40 dvcs.2016.P07.mu.plus.408.tar
-rw-rw-r--+  1 akoval G-821143        49291 May  8 06:58 dvcs.2016.P07.mu.plus.408.tar.content
-rw-rw-r--+  1 akoval G-821143            0 May  7 05:42 dvcs.2016.P07.mu.plus.408.tar.bad
-rw-rw-r--+  1 akoval G-821143        49291 May  8 06:58 dvcs.2016.P07.mu.plus.408.tar.log
-rw-rw-r--+  1 akoval G-821143           44 May  7 02:02 dvcs.2016.P07.mu.plus.408.stderr
-rw-rw-r--+  1 akoval G-821143           44 May  7 05:42 dvcs.2016.P07.mu.plus.408.stdout
```
if tarring failed (indicated by presence of `dvcs.2016.P07.mu.plus.408.tar.bad`). For more information examine corresponding `.stdout`, `.stderr` and `.tar.log`.

